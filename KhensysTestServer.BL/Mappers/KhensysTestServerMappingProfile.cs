﻿using AutoMapper;
using KhensysTestServer.BL.Dtos;
using KhensysTestServer.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.BL.Mappers
{
    public class KhensysTestServerMappingProfile : Profile
    {
        public KhensysTestServerMappingProfile()
        {
            CreateMap<PermissionType, PermissionTypeDto>()
            .ReverseMap();

            CreateMap<Permission, PermissionDto>()
            .ReverseMap();
        }
    }
}