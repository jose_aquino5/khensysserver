﻿using KhensysTestServer.BL.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.BL.Dtos
{
    public class PermissionDto : IEntityBaseDto
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeLastName { get; set; }
        public int PermissionTypeId { get; set; }
        public DateTime PermissionDate { get; set; }
        public virtual PermissionTypeDto PermissionType { get; set; }
    }
}
