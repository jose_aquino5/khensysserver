﻿using KhensysTestServer.BL.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.BL.Dtos
{
    public class PermissionTypeDto : IEntityBaseDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
