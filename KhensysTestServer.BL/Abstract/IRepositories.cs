﻿using KhensysTestServer.Core;
using KhensysTestServer.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.BL.Abstract
{
    public interface IPermissionTypeRepository : IEntityBaseRepository<PermissionType> { }
    public interface IPermissionRepository : IEntityBaseRepository<Permission> { }
}
