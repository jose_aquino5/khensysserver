﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.BL.Abstract
{
    public interface IEntityBaseDto
    {
        int Id { get; set; }
    }
}
