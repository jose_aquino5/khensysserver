﻿using FluentValidation;
using KhensysTestServer.DataModel.Context;
using KhensysTestServer.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KhensysTestServer.BL.Validators
{
    public class PermissionTypeValidator : AbstractValidator<PermissionType>
    {
        protected readonly KhensysTestServerContext _context;
        public PermissionTypeValidator(KhensysTestServerContext context)
        {
            _context = context;

            RuleFor(entity => entity.Description)
                .NotEmpty()
                .Must(IsUnique).WithMessage("The field description must be unique");
        }

        public bool IsUnique(PermissionType permissionType, string description) 
        {
            if (permissionType.Id != 0) return true;
            var IsUnique = !_context.PermissionTypes.Any(pt => pt.Description.Equals(description));
            return IsUnique;
        }
    }
}
