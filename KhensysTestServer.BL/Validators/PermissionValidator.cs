﻿using FluentValidation;
using KhensysTestServer.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.BL.Validators
{
    public class PermissionValidator : AbstractValidator<Permission>
    {
        public PermissionValidator()
        {
            RuleFor(entity => entity.EmployeeName)
                .NotEmpty();

            RuleFor(entity => entity.EmployeeLastName)
                .NotEmpty();            
            
            RuleFor(entity => entity.PermissionTypeId)
                .NotEmpty();

            RuleFor(entity => entity.PermissionDate)
                .NotEmpty();
        }
    }
}
