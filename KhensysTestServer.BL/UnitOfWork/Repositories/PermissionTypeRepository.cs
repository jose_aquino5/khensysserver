﻿using FluentValidation;
using KhensysTestServer.BL.Abstract;
using KhensysTestServer.DataModel.Context;
using KhensysTestServer.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.BL.UnitOfWork.Repositories
{
    public class PermissionTypeRepository : EntityBaseRepository<PermissionType>, IPermissionTypeRepository
    {
        public PermissionTypeRepository(KhensysTestServerContext context, IValidator<PermissionType> validator)
        : base(context, validator)
        {

        }
    }
}