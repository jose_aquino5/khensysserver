﻿using FluentValidation;
using KhensysTestServer.BL.Abstract;
using KhensysTestServer.DataModel.Context;
using KhensysTestServer.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.BL.UnitOfWork.Repositories
{
    public class PermissionRepository : EntityBaseRepository<Permission>, IPermissionRepository
    {
        public PermissionRepository(KhensysTestServerContext context, IValidator<Permission> validator)
        : base(context, validator)
        {

        }
    }
}