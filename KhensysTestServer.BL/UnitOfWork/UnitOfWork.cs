﻿using KhensysTestServer.Core;
using KhensysTestServer.DataModel.Context;
using Lamar;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KhensysTestServer.BL.UnitOfWork
{
    public partial class UnitOfWork
    {
        private readonly IContainer _container;

        public UnitOfWork(IContainer container)
        {
            _container = container;
        }

        public IEntityBaseRepository<TEntity> Get<TEntity>() where TEntity : class, IEntityBase, new()
        {
            return _container.GetInstance<IEntityBaseRepository<TEntity>>();
        }
        public async Task SaveAsync()
        {
            try
            {
                await _container.GetInstance<KhensysTestServerContext>().SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
                //Log the error (uncomment ex variable name and write a log.)
                //ModelState.AddModelError("", "Unable to save changes. " +
                //"Try again, and if the problem persists, " +
                // "see your system administrator.");
            }
        }
    }
}