﻿using KhensysTestServer.DataModel.Context;
using System;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using System.Linq;
using KhensysTestServer.DataModel.SampleData;

namespace KhensysTestServer.DataModel
{
    public static class DbInitializer
    {
        public async static Task Init(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<KhensysTestServerContext>();
            await context.Database.EnsureCreatedAsync();

            if (!context.PermissionTypes.Any())
            {
                await context.PermissionTypes.AddRangeAsync(PermissionTypeSample.PermissionTypes);
                await context.SaveChangesAsync();
            }

            if (!context.Permissions.Any())
            {
                Random random = new Random();
                foreach (var permission in PermissionSample.Permissions)    
                {
                    int index = random.Next(context.PermissionTypes.Count());
                    permission.PermissionTypeId = context.PermissionTypes.ToList().ElementAtOrDefault(index).Id;
                }
                await context.Permissions.AddRangeAsync(PermissionSample.Permissions);
                await context.SaveChangesAsync();
            }
        }
    }
}