﻿using KhensysTestServer.Core;
using KhensysTestServer.Core.Extensions;
using KhensysTestServer.DataModel.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KhensysTestServer.DataModel.Context
{
    public class KhensysTestServerContext : DbContext
    {
        public KhensysTestServerContext(DbContextOptions<KhensysTestServerContext> options) : base(options)
        {

        }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<PermissionType> PermissionTypes { get; set; }


        #region OnModelCreating
        /// <summary>
        /// Override OnModelCreating so we can perform operations on the ModelBuilder object.
        /// </summary>
        /// <returns></returns>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            /* NOTE: Through reflection, all the classes that implement 
             * "IEntityTypeConfiguration <>" are read and then they are registered 
             * to be created by Entity Framework. */
            foreach (var type in Assembly.GetExecutingAssembly().GetTypes()
                                  .Where(x => x.GetInterfaces().Any(y => y.GetTypeInfo().IsGenericType && y.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>))))
            {
                var hasConstructorParams = type.GetConstructors().Any(c => c.GetParameters().Any(p => p.ParameterType == typeof(KhensysTestServerContext)));
                dynamic configurationInstance = hasConstructorParams ? Activator.CreateInstance(type, this) : Activator.CreateInstance(type);
                modelBuilder.ApplyConfiguration(configurationInstance);
            }

            /* NOTE: A query filter is applied to all the entities that inherit from 
             * IEntidadAuditableBase with the objective that when a result of the 
             * database is obtained, it automatically ignores the records that have 
             * the true value in "Borrado" field. */
            foreach (var type in modelBuilder.Model.GetEntityTypes()
                                .Where(type => typeof(IEntityAuditableBase).IsAssignableFrom(type.ClrType)))
            {
                modelBuilder.SetSoftDeleteFilter(type.ClrType);
            }

            /* NOTE: EF Core by default applies the cascade delete behavior, 
             * in this section that behavior is altered so that it does not allow 
             * cascade delete of any record.*/
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
        #endregion

        #region SaveChanges
        /// <summary>
        /// Override SaveChanges so we can call the new AuditEntities method.
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            this.AuditEntities();

            return base.SaveChanges();
        }
        #endregion

        #region SaveChangesAsync
        /// <summary>
        /// Override SaveChanges so we can call the new AuditEntities method.
        /// </summary>
        /// <returns></returns>
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            this.AuditEntities();

            return base.SaveChangesAsync(cancellationToken);
        }
        #endregion

        #region AuditEntities
        /// <summary>
        /// Method that will set the Audit properties for every added or modified Entity marked with the 
        /// IAuditable interface.
        /// </summary>
        private void AuditEntities()
        {

            // For every changed entity marked as "IEntidadAuditableBase" set the values for the audit properties
            foreach (EntityEntry<IEntityAuditableBase> entry in ChangeTracker.Entries<IEntityAuditableBase>())
            {
                if (entry.State == EntityState.Added) // If the entity was added.
                {
                    entry.Entity.CreatedDate = DateTime.Now;
                }
                else if (entry.State == EntityState.Modified) // If the entity was updated
                {
                    entry.Entity.ModifiedDate = DateTime.Now;
                    Entry(entry.Entity).Property(x => x.CreatedDate).IsModified = false;
                }
            }
        }
        #endregion
    }
}
