﻿using KhensysTestServer.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.DataModel.SampleData
{
    public class PermissionTypeSample
    {
        private static List<PermissionType> _permissionTypes;

        static PermissionTypeSample()
        {
            if (_permissionTypes == null)
            {
                PermissionTypes = new List<PermissionType>()
                {
                    new PermissionType { Description = "Salud" },
                    new PermissionType { Description = "Matrimonio" },
                    new PermissionType { Description = "Fallecimiento" },
                    new PermissionType { Description = "Mundanza" },
                    new PermissionType { Description = "Estudios" },
                };
            }
        }
        public static List<PermissionType> PermissionTypes { get => _permissionTypes; set => _permissionTypes = value; }

    }
}
