﻿using KhensysTestServer.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.DataModel.SampleData
{
    public class PermissionSample
    {
        private static List<Permission> _permissions;

        static PermissionSample()
        {
            if (_permissions == null)
            {
                var random = new Random();
                Permissions = new List<Permission>()
                {
                    new Permission { EmployeeName = "Manuel", EmployeeLastName = "Batista Baez", PermissionDate = DateTime.Now.AddDays(random.Next(1, 15)) },
                    new Permission { EmployeeName = "Carlos", EmployeeLastName = "Fernandez Rodriguez", PermissionDate = DateTime.Now.AddDays(random.Next(1, 15)) },
                    new Permission { EmployeeName = "Patricia", EmployeeLastName = "Mendez Barias", PermissionDate = DateTime.Now.AddDays(random.Next(1, 15)) },
                    new Permission { EmployeeName = "Juan", EmployeeLastName = "García Gonzales", PermissionDate = DateTime.Now.AddDays(random.Next(1, 15)) },
                    new Permission { EmployeeName = "Pedro", EmployeeLastName = "Bargas Prado", PermissionDate = DateTime.Now.AddDays(random.Next(1, 15)) },
                };
            }
        }
        public static List<Permission> Permissions { get => _permissions; set => _permissions = value; }

    }
}
