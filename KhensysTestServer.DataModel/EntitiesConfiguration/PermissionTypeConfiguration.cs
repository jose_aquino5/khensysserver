﻿using KhensysTestServer.DataModel.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.DataModel.EntitiesConfiguration
{
    public class PermissionTypeConfiguration : IEntityTypeConfiguration<PermissionType>
    {
        public void Configure(EntityTypeBuilder<PermissionType> builder)
        {
            builder.ToTable("TipoPermiso");

            builder.Property(e => e.Description)
                    .HasColumnName("Descripcion")
                    .HasMaxLength(256)
                    .IsRequired();
        }
    }
}
