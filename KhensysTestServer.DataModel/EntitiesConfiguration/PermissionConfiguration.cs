﻿using KhensysTestServer.DataModel.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;


namespace KhensysTestServer.DataModel.EntitiesConfiguration
{
    public class PermissionConfiguration : IEntityTypeConfiguration<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> builder)
        {
            builder.ToTable("Permiso");

            builder.Property(e => e.EmployeeName)
                    .HasColumnName("NombreEmpleado")
                    .HasMaxLength(256)
                    .IsRequired();

            builder.Property(e => e.EmployeeLastName)
                    .HasColumnName("ApellidosEmpleado")
                    .HasMaxLength(256)
                    .IsRequired();

            builder.Property(e => e.PermissionTypeId)
                    .HasColumnName("TipoPermiso");

            builder.Property(e => e.PermissionDate)
                    .HasColumnName("FechaPermiso")
                    .IsRequired();
        }
    }
}