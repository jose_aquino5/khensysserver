﻿using KhensysTestServer.Core;
using System;
using System.Collections.Generic;
using System.Text;


namespace KhensysTestServer.DataModel.Entities
{
    public class PermissionType : IEntityAuditableBase
    {
        public int Id { get; set; }
        public string Description { get; set; }

        #region AuditFields
        /// <summary>
        /// Bool property represent if the cut was deleted by user
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Date property representing the date on which the entity was created
        /// </summary>
        public DateTimeOffset CreatedDate { get; set; }

        /// <summary>
        /// Date property representing the date on which the entity was edited
        /// </summary>
        public DateTimeOffset? ModifiedDate { get; set; }

        #endregion
    }
}
