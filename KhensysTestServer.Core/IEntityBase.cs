﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.Core
{
    public interface IEntityBase
    {
        int Id { get; set; }
        bool IsDeleted { get; set; }
    }
}
