﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace KhensysTestServer.Core.Extensions
{
    public static class MemberInfoExtensions
    {
        public static string GetCleanNameFromDto(this MemberInfo memberInfo)
        {
            return memberInfo.Name.Replace("Dto", "");
        }
    }
}
