﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.Core.Exceptions
{
   public class DeleteFailureException : Exception
    {
        public DeleteFailureException(string message) : base(message)
        {
        }
    }
}
