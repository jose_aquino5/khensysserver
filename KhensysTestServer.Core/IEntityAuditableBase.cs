﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KhensysTestServer.Core
{
    public interface IEntityAuditableBase : IEntityBase
    {
        DateTimeOffset CreatedDate { get; set; }
        DateTimeOffset? ModifiedDate { get; set; }
    }
}
