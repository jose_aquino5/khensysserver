using AutoMapper;
using KhensysTestServer.BL.Abstract;
using KhensysTestServer.BL.Mappers;
using KhensysTestServer.BL.UnitOfWork;
using KhensysTestServer.BL.UnitOfWork.Repositories;
using KhensysTestServer.DataModel.Context;
using Lamar;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KhensysTestServer.Api.Filters;

namespace KhensysTestServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureContainer(ServiceRegistry services)
        {
            // Supports ASP.Net Core DI abstractions
            services.AddLogging();

            services.AddTransient<UnitOfWork, UnitOfWork>();
            // Also exposes Lamar specific registrations
            // and functionality
            services.Scan(s =>
            {
                s.TheCallingAssembly();
                s.AssemblyContainingType<UnitOfWork>();
                s.WithDefaultConventions();
            });
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("_myAllowSpecificOrigins",
                                  builder =>
                                  {
                                      builder.WithOrigins("http://localhost:8080")
                                            .AllowAnyHeader()
                                            .AllowAnyMethod();
                                  });
            });

            services.AddControllers(options => {
                options.Filters.Add(typeof(CustomExceptionFilterAttribute));
            }).AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<UnitOfWork>());

            #region FluentValidation 
            // Add entity framework services.
            services.AddDbContext<KhensysTestServerContext>(options =>
            {
                // Configure the context to use Microsoft SQL Server.
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly("KhensysTestServer.Api"));
            });
            #endregion

            #region AutoMapper  
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<KhensysTestServerMappingProfile>();
            });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("_myAllowSpecificOrigins");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseWelcomePage();
        }
    }
}
