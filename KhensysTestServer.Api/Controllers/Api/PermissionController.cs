﻿using AutoMapper;
using KhensysTestServer.BL.Dtos;
using KhensysTestServer.BL.UnitOfWork;
using KhensysTestServer.DataModel.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhensysTestServer.Api.Controllers.Api
{
    public class PermissionController : BaseApiController<Permission, PermissionDto>
    {
        public PermissionController(UnitOfWork unitOfWork, IMapper mapper)
            : base(unitOfWork, mapper)
        {

        }

        [HttpGet]
        public override IActionResult Get()
        {
            return Ok(_mapper.Map<List<PermissionDto>>(_repository.GetAll(p=>p.PermissionType)));
        }

    }
}