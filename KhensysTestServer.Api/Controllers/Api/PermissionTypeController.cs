﻿using AutoMapper;
using KhensysTestServer.BL.Dtos;
using KhensysTestServer.BL.UnitOfWork;
using KhensysTestServer.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KhensysTestServer.Api.Controllers.Api
{
    public class PermissionTypeController : BaseApiController<PermissionType, PermissionTypeDto>
    {
        public PermissionTypeController(UnitOfWork unitOfWork, IMapper mapper)
            : base(unitOfWork, mapper)
        {

        }
    }
}